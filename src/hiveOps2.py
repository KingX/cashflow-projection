import os
from pyhive import hive
import pandas as pd
from time import time
#from utils.optimizers import optimize
import argparse

class hiveOps:
  def __init__(
    self,
    host=os.environ['MASTER_NODE_HOST'],
    port=os.environ['HIVE_PORT'],
    user=os.environ['HIVE_USER'],
    operation='select'
  ):
    """
    Sets default parameters for the class. The operation paremeter is set to 'select' by default to execute select query.
    However, if the user running insert or delete query, the user must set the operation to 'insert' or 'dete'.
    """
    
    self.host=host
    self.port=int(port)
    self.user=user
    self.operation=operation
    
  def create_hive_conn(self):
    if self.operation == 'insert' or self.operation == 'delete':
      conn = hive.Connection(host=self.host,
                             port=self.port,
                             username=self.user,
                             configuration={
                               'hive.support.concurrency':'true',
                               'hive.txn.manager':'org.apache.hadoop.hive.ql.lockmgr.DbTxnManager',
                               'hive.enforce.bucketing':'true',
                               'hive.exec.dynamic.partition.mode':'nostrict',
                               'hive.compactor.initiator.on':'true',
                               'hive.execution.engine':'mr'
                             }
                            )
    elif self.operation == 'select':
      conn = hive.Connection(host=self.host,
                             port=self.port,
                             username=self.user,
                             configuration={
                               'hive.support.concurrency':'true',
                               'hive.txn.manager':'org.apache.hadoop.hive.ql.lockmgr.DbTxnManager',
                               'hive.enforce.bucketing':'true',
                               'hive.exec.dynamic.partition.mode':'nostrict',
                               'hive.compactor.initiator.on':'true',
                               'hive.execution.engine':'tez'
                             }
                            )
    else:
      print('Please specify which operation to use')
      conn = None
    return conn
  
  def execute_query(self, query, cols=None):
    
    conn = self.create_hive_conn()
    
    try:
      cursor = conn.cursor()
      cursor.execute(query)
      cursor.close()
      conn.commit()
      conn.close()
    except Exception as e:
      conn.close()
      raise e