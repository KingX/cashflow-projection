import os
import pandas as pd
import argparse
from impala.dbapi import connect
from impala.util import as_pandas

from utils.optimizers import optimize

class hiveOps:
  def __init__(
    self,
    host=os.environ["MASTER_NODE_HOST"],
    port=os.environ["HIVE_PORT"],
    user=os.environ["HIVE_USER"],
    password=os.environ["HIVE_PASSWORD"],
    service='hive'
  ):
    """
    Sets default parameters for the class. Everything is self-explanatory except the 'service'.
    Ideally, set the service to 'impala' if you're running a SELECT query only.
    """
    
    self.host = host
    self.port = int(port)
    self.user = user
    self.password = password
    self.service = service
    

  def create_hive_conn(self, db):
    """
      Creates a connection to the Hive data warehouse and returns a connection object

      Arguments:
        db --> Name of the database to connect to
    """

    try:
      # This connection string depends on your cluster setup and authentication mechanism
      conn = connect(host=self.host,
         port=self.port,
         auth_mechanism='PLAIN',
         database=db,
         user=self.user,
         password=self.password,
         kerberos_service_name=self.service
      )
      return conn
    except Exception as e:
      raise e

  def execute_query(self, db, query, cols=None):
    """
      Reuses a Hive connection object executes runs any kind of Hive or Impala query.
      Returns a pandas dataframe if the query is SELECT, otherwise None.

      Arguments:
        db --> Name of the database to connect to
        query --> query string to be executed in Hive
        cols (optional) --> column names to be returned for df
    """

    conn = self.create_hive_conn(db)

    #execute query and close connection
    try:
      cursor = conn.cursor(convert_types=False)
      cursor.execute(query)
      print('DONE')
      
      if 'select' in query.lower():
        print(cursor.fetchmany(10))
#        df = as_pandas(cursor)
#        df = optimize(pd.DataFrame(results))
        #rename df columns if passed
        if cols is not None:
          df.columns = cols
        conn.commit()
        cursor.close()
        conn.close()
        return df
      else:
        conn.commit()
        cursor.close()
        conn.close()
        return None
      
    except Exception as e:
      cursor.close()
      conn.close()
      raise e