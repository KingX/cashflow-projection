import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt

import os

def powerbi_transformation(historical, predictions):
  standard_cols = ['date', 'value', 'flag']
  
  historical['date'] = pd.to_datetime(historical['date'])
  predictions['ds'] = pd.to_datetime(predictions['ds'])
  
  historical = historical[['date', 'cashflow']]
  historical['flag'] = 'historical'
  
  forecast = predictions[-30:][['ds', 'yhat']]
  forecast['flag'] = 'forecast'
  
  upper = predictions[-30:][['ds', 'yhat_upper']]
  upper['flag'] = 'upper'
  
  lower = predictions[-30:][['ds', 'yhat_lower']]
  lower['flag'] = 'lower'
  
  historical.columns = standard_cols
  forecast.columns = standard_cols
  upper.columns = standard_cols
  lower.columns = standard_cols
  
  new_forecast = forecast.append(historical[-1:])
  new_forecast['flag'] = new_forecast['flag'].replace(['historical', 'forecast'])

  new_upper = upper.append(historical[-1:])
  new_upper['flag'] = new_upper['flag'].replace(['historical', 'upper'])

  new_lower = lower.append(historical[-1:])
  new_lower['flag'] = new_lower['flag'].replace(['historical', 'lower'])
  
  powerbi_cashflow = pd.concat([new_forecast, new_upper, new_lower, historical], axis=0)
  sns.lineplot(data=powerbi_cashflow, x='date', y='value', hue='flag')
  return powerbi_cashflow
  
  
fpath = '/home/cdsw/data'
h = 'cashflow.csv'
p = 'predictions.csv'

historical = pd.read_csv(os.path.join(fpath, h))
predictions = pd.read_csv(os.path.join(fpath, p))

powerbi_cashflow = powerbi_transformation(historical, predictions)
powerbi_cashflow.to_csv(os.path.join(fpath, 'powerbi_cashflow.csv'), index=False)