import os
import json
import matplotlib.pyplot as plt

from fbprophet import Prophet
from fbprophet.serialize import model_to_json, model_from_json

def predict(model, n):
  forecast = model.make_future_dataframe(periods=n, freq='D')
  predictions = model.predict(forecast)
  figure = model.plot(predictions)
  plt.title('Cash Flow Projection', fontsize=25)
  return predictions

model_path = '/home/cdsw/model'

# load model
with open(os.path.join(model_path, 'serialized_model.json'), 'r') as fin:
    model = model_from_json(json.load(fin))
    
predictions_path = '/home/cdsw/data'

predictions = predict(model, 30)
predictions.to_csv(os.path.join(predictions_path, 'predictions.csv'), index=False)