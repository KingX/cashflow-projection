import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

import os
import json
import datetime

from fbprophet import Prophet
from fbprophet.serialize import model_to_json, model_from_json

def train_model(data, holidays=False, ws=False, ys=False, ds=False, ms=False, iw=0.85):
  ts = data
  ts.columns = ['ds', 'y']
  if isinstance(holidays, pd.DataFrame):
    model = Prophet(weekly_seasonality=ws, yearly_seasonality=ys, daily_seasonality=ds, interval_width=iw, holidays=holidays)
  else:
    model = Prophet(weekly_seasonality=ws, yearly_seasonality=ys, daily_seasonality=ds, interval_width=iw)
  if ms:
    model.add_seasonality(name='monthly', period=30.5, fourier_order=5)
  model = model.fit(ts)
  return model

def create_holidays(start_year, end_year):
  holiday_df = []
  holidays = [("New Year's Day", '-01-01'), ("Maundy Thursday", '-04-01'), ("Good Friday", '-04-02'), 
          ("Araw ng Kagitingan", '-04-09'), ("Labor Day", '-05-01'), ("Independence Day", '-06-12'), 
          ("National Heroes’ Day", '-08-30'), ("Bonifacio Day", '-11-30'), ("Christmas Day", '-12-25'),
          ("Rizal Day", '-12-30')]
            
  for year in range(start_year, end_year+1, 1):
      for h in holidays:
          holiday_df.append({
            'ds':str(year)+h[1],
            'holiday':h[0],
            'lower_window': 0,
            'upper_window': 0,
          })

  holiday_df = pd.DataFrame(holiday_df)
  holiday_df['ds'] = pd.to_datetime(holiday_df.ds)
  return holiday_df

fpath = '/home/cdsw/data'
f = 'cashflow.csv'

data = pd.read_csv(os.path.join(fpath, f))
data = data[data['year'] != 2020]
data = data[['date', 'cashflow']]
data['date'] = pd.to_datetime(data['date'])

holidays = create_holidays(2018, 2022)

model = train_model(data, ws=True, ms=True, iw=0.95, holidays=holidays)

# save model
model_path = '/home/cdsw/model'
with open(os.path.join(model_path,'serialized_model.json'), 'w') as fout:
    json.dump(model_to_json(model), fout)