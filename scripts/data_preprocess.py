import pandas as pd
import numpy as np
import os

def pre_process(fpath, f):
  cashflow = pd.read_csv(os.path.join(fpath, f))
  cashflow['date'] = pd.to_datetime(cashflow['date'])
  cashflow = cashflow.drop_duplicates(subset=['date' , 'net_loan'], keep='first')
  cashflow['year'] = cashflow['date'].dt.year
  cashflow['month'] = cashflow['date'].dt.month
  cashflow = cashflow[['date', 'net_loan', 'year', 'month']]
  cashflow.columns = ['date', 'cashflow', 'year', 'month']
  return cashflow

fpath = '/home/cdsw/data'
f = 'cash_flow_202102191035.csv'
data = pre_process(fpath, f)
data.to_csv(os.path.join(fpath, 'cashflow.csv'), index=False)